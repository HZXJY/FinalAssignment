package Object;

import SQL_table.Users_attr;
import java.util.ArrayList;

public class User extends Users_attr {
    public User(String userID, String loginName, String password, String userType) {
        super(userID, loginName, password, userType);
    }

    public String clientTitle=null;
    public ArrayList clientData=null;
    /*
    * 用于各个函数之间传递用户信息和用户向服务器发送报文的通用格式
    * clientRequest为客户端发送的请求，仅在报文中使用
    * clientData为客户端发送的数据，仅在报文中使用
     */
}
