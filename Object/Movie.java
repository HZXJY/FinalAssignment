package Object;

import SQL_table.Movie_attr;

public class Movie extends Movie_attr {
    public Movie(String movieID, String movieName, String director, String mainActor, String moviePoster, String price) {
        super(movieID, movieName, director, mainActor, moviePoster, price);
    }
}
