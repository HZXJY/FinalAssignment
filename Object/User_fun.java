package Object;
/*
* 所有用户类的父类
 */
public class User_fun extends User{
    public User_fun(String userID, String loginName, String password, String userType) {
        super(userID, loginName, password, userType);
    }

    private void quit(){
        //登出，关闭线程
    }

    private boolean changePasswd(){
        //修改密码
        return false;
    }

    private boolean changeUsername(){
        //更改用户名
        return false;
    }


}
