package Object;
/*
* 实现顾客方法的类
 */
public class User_Customer_fun extends User_fun{
    public User_Customer_fun(String userID, String loginName, String password, String userType) {
        super(userID, loginName, password, userType);
    }

    private boolean showMovies(String ScheduleTime){
        //查看某日正在放映的电影
        return false;
    }

    private boolean showMovieDetail(String movieID){
        //查看movieID所属的场次、座位
        return false;
    }

    private boolean selectSeat(int Row,int Columns,int TheaterID,int MovieID){
        //选取场次、座位
        return false;
    }

    private boolean buy(Ticket ticket){
        //买！
        return false;
    }

    private boolean refund(String ticketID){
        //退款
        return false;
    }

    private Ticket showTicketData(String ticketID){
        //向客户端返回票务信息
        return null;
    }
}
