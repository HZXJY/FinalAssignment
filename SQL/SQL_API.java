package SQL;
import SQL_table.Table;
import java.util.ArrayList;

public interface SQL_API {
    boolean connectDatabase();
    /*
    * 连接数据库
     */

    boolean SQL_userLogin(String userName, String passwd);
    /*
    * 数据库登录，同时切换到theaterDB
     */

    boolean SQL_insert(Table tableName, ArrayList<String> insertValue);
    /*
    * 使用SQL中的insert语句向表中插入数据
    * tableName为表名，insertValue为被插入表的属性
    * ArrayList值为所需要插入的值
    * 若HashMap某键所对应的值为NULL，则代表不需要插入该属性对应的值
     */

    ArrayList<String> SQL_select(Table tableName, ArrayList<String> selectValue);
    /*
    * 使用SQL中的select语句从表中查询数据
    * tableName为表名，selectValue为所需要查询数据的属性
    * 由于函数向表查询数据，所以tableValue的值可以是任意数据
    * 函数返回ArrayList，如果返回的ArrayList为null，则代表查询失败
     */

    boolean SQL_delete(Table tableName, ArrayList<String> deleteValue);
    /*
    * 使用SQL中的delete语句从表中删除数据
    * tableName为表名，deleteValue为被删除数据的属性
    * tableValue的值用于delete中的选择条件，null表示该键所表示的属性不参与delete条件选择
    * 检查tableValue是否全为null，全为null则函数返回false删除失败
    * 全为null会把整张表删除！！！整张表删除！！！
     */

    boolean SQL_update(Table tableName, ArrayList<String> searchLabel, ArrayList<String> updateValue);
    /*
    * 使用SQL中的update语句从表中修改数据
    * tableName为表名，searchLabel为被删除数据的属性,updateValue为需要更新的数据
    * searchLabel的值用于update中的选择条件，null表示该键所表示的属性不参与update条件选择
    * updateValue的键值为“NULL”的时候表示该键的属性不作为update的插入数据，如果键值为null则表示向表中插入值null
     */

    boolean customize(String SQLScript);
    /*
    * 向数据库输入SQL命令，仅管理员可用
     */

}
