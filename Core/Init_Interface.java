package Core;
import Object.User;
import java.util.concurrent.SynchronousQueue;

public interface Init_Interface {
    boolean initialize();
    /*
    * 初始化服务端的函数，检查文件Theater_ticket_system.xml的状态，并决定系统的下一步操作
    * 如果没有该文件（即用户第一次使用该系统），则在文件夹中创建Theater_ticket_system.xml
    * 并建立数据库SQLInit()和创建管理员用户userInit()
    * 如果Theater_ticket_system.xml存在，则不做任何操作
    */

    boolean SQLInit();
    /*
     * 连接数据库
     */

    boolean webInit();
    /*
    * 创建服务端等待连接
     */

    Cache cache= new Cache();
    /*
    * 创建数据缓存
     */

    static SynchronousQueue<User> messageQueue=new SynchronousQueue<User>();
    /*
    * 用于主线程和网络连接线程之间通信的消息队列
     */
}
