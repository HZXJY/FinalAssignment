package Core;
import Object.User;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import static Core.Init_Interface.messageQueue;

public class Web_link extends Thread{
    User weblink=new User("0","weblink",null,"0");
    @Override
    public void run() {
        try {
            System.out.println("start");
            ServerSocket serverSocket=new ServerSocket(2333);
            weblink.clientTitle="finish";
            messageQueue.put(weblink);//向主线程发送线程创建成功的消息
            Socket socket=serverSocket.accept();//与客户端连接
            DataInputStream dataInputStream=new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream=new DataOutputStream(socket.getOutputStream());
            InetAddress inetAddress=socket.getInetAddress();//创建相应的流
            while (true) {
                String a = dataInputStream.readUTF();
                System.out.println(a);
            }
            //System.out.println("success");
        } catch (IOException e) {
            //e.printStackTrace();
            try {
                weblink.clientTitle="failed";
                messageQueue.put(weblink);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }//嵌套try/catch检测队列异常
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

/*
* ——为什么会有这个文件？
* 原计划为可允许多用户登入系统
* 然而多线程难度太大
* 改为了仅单个用户登入
 */
