package Core;
import Object.*;

public interface Core_interface {
    User emptyUser =new User(null,null,null,null);//空用户
    void spin();
    /*
    * 仅一次执行Init
    * 进行无限循环，每隔一定时间调用monitor()
    * 如果monitor()返回用户请求，则调用checkUserState()进行检查
    * 调用treadControl()向线程传递消息
     */

     User monitor();
    /*
     * 监听客户端发送请求
     * 当检测到服务请求时，返回类User
     * 若没有请求这返回emptyUser
     */

    boolean monitor(User user);
    /*
    * 向客户端发送返回的消息
    * 根据是否发送成功返回bool值
     */

    boolean checkUserState();
    /*
    * 使用Cache.checkPasswd检查用户的合法性
    * 如果合法，则使用Cache.checkPid检查用户是否已有线程服务
    *   如果未有线程，则返回0
    *   如果已有线程，则返回1
    * 如果不合法，则返回-1
     */

    //void treadControl(int flag);
    /*
     * flag=0,则使用fork()创建子线程
     * flag=1,则使用notify()唤醒线程
     * 将monitor()接收到的USER类传递给相应用户的子线程
     */

    //int fork(User_fun user);
    /*
    * 创建线程用于处理用户请求
    * 创建成功则返回pid,并使用wait()挂起线程
    * 将用户id与线程号进行映射写入Core.Cache中
    * 错误则返回-1
    */


    Cache cache= new Cache();
    /*
     * 创建数据缓存
     */

    Init init=new Init();
    /*
    * 创建初始化对象
     */

}
