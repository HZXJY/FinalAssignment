package Core;
import Object.User;
public class Init implements Init_Interface {
    @Override
    public boolean initialize() {
        System.out.println("连接数据库……");
        if(SQLInit()){
            System.out.println("    数据库连接成功");
        }
        else{
            System.out.println("    初始化失败，原因：数据库连接失败");
            return false;
        }
        System.out.println("创建网络接口……");
        if(webInit()){
            System.out.println("    网络接口创建成功");
        }
        else{
            System.out.println("    初始化失败，原因：网络接口创建失败");
            return false;
        }
        System.out.println("系统初始化成功");
        return true;
    }

    @Override
    public boolean SQLInit() {
        return true;
    }

    @Override
    public boolean webInit() {
        try {
            Web_link webConnect=new Web_link();
            webConnect.start();
            User message=messageQueue.take();//从子线程中获得异常消息
            if(message.clientTitle.equals("finish"))
                return true;
            if(message.clientTitle.equals("failed"))
                return false;
            else
                return false;
        } catch (InterruptedException e) {
            return false;
        }
    }


    public static void main(String[] args){
        Init init=new Init();
        init.initialize();
    }
}


