package Core;
import Object.User;
import java.util.HashMap;
/*
* 用于保存用户名与密码和线程号的缓存
 */
public class Cache {
    private HashMap<String,String> PidCache;//对用户id与线程号进行映射
    private HashMap<String,String> passwdCache;//对用户id与密码进行映射

    public boolean addPidMapping(User infor,String pid){
        return false;
    }
    /*
    * 加入userID与pid的映射
    * 仅限Core.fork()使用！！
     */

    public String checkPid(User infor){
        return null;
    }
    /*
    * 使用userID对PidCache进行查询，如果存在则返回pid
    * 如果不存在则返回null
     */

    public boolean checkPasswd(User imfor){
        return false;
    }
    /*
    * 使用userID对passwdCache进行查询，如果存在则返回true
    * 如果不存在，则使用userID对数据库进行查询，如果存在则返回true，并将userID与passwd的映射写入passwdCache中
    * 如果passwdCache与数据库都不存在，则返回false
     */
}
