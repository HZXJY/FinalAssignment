package SQL_table;

import java.util.HashMap;

public class Ticket_attr {
    private String TicketID;
    private String Row;
    private String Columns;
    private String ScheduleID;
    private String Status;

    public Ticket_attr(String ticketID, String row, String columns, String scheduleID, String status) {
        TicketID = ticketID;
        Row = row;
        Columns = columns;
        ScheduleID = scheduleID;
        Status = status;
    }

    public HashMap returnAttrCollect() {
        HashMap<String, String> RAC = new HashMap<String, String>();
        RAC.put("TicketID", TicketID);
        RAC.put("Row", Row);
        RAC.put("Columns", Columns);
        RAC.put("ScheduleID", ScheduleID);
        RAC.put("Status", Status);
        return RAC;
    }
}
