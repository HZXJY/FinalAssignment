  
##数据库的数据结构：
###<em>该包中的类只允许在数据库相关函数中使用！<em>
+ database
    + Schedule
        + <font color=GREEN><Strong><em>ScheduleID</Strong></font></em>
        + ScheduleTime
        + <font color=ORANGE>MovieID</font>
        + <font color=BLUE>TheaterID</font>
  
    + Ticket
        + <em><Strong>TicketID</Strong></em>
        + Row
        + Columns
        + <font color=GREEN>ScheduleID</font>
        + Status
    
    + Theater
        + <Strong><font color=BLUE><em>TheaterID</Strong></font></em>
        + TheaterName
        + Capacity
  
    + Movie
        + <font color=ORANGE><Strong><em>MovieID</em></Strong></font>
        + MovieName
        + Director
        + MainActor
        + MoviePoster
        + Price
  
    + Users
        + <Strong><em>UserID</em></Strong>
        + LoginName
        + Password
        + UserType