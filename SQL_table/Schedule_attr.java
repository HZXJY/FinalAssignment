package SQL_table;

import java.util.HashMap;

public class Schedule_attr {
    private String ScheduleID;
    private String ScheduleTime;
    private String MovieID;
    private String TheaterID;

    public Schedule_attr(String scheduleID, String scheduleTime, String movieID, String theaterID) {
        ScheduleID = scheduleID;
        ScheduleTime = scheduleTime;
        MovieID = movieID;
        TheaterID = theaterID;
    }

    public HashMap returnAttrCollect() {
        HashMap<String, String> RAC = new HashMap<String, String>();
        RAC.put("ScheduleID", ScheduleID);
        RAC.put("ScheduleTime", ScheduleTime);
        RAC.put("MovieID", MovieID);
        RAC.put("TheaterID", TheaterID);
        return RAC;
    }
}
