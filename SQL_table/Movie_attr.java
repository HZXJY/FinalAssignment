package SQL_table;

import java.util.HashMap;

public class Movie_attr {
    private String MovieID;
    private String MovieName;
    private String Director;
    private String MainActor;
    private String MoviePoster;
    private String Price;

    public Movie_attr(String movieID, String movieName, String director, String mainActor, String moviePoster, String price) {
        MovieID = movieID;
        MovieName = movieName;
        Director = director;
        MainActor = mainActor;
        MoviePoster = moviePoster;
        Price = price;
    }

    public HashMap returnAttrCollect() {
        HashMap<String, String> RAC = new HashMap<String, String>();
        RAC.put("MovieID",MovieID);
        RAC.put("MovieName",MovieName);
        RAC.put("Director",Director);
        RAC.put("MainActor",MainActor);
        RAC.put("Movie_poster",MoviePoster);
        RAC.put("Price",Price);
        return RAC;
    }
}
