package SQL_table;

import java.util.HashMap;

public class Theater_attr {
    private String TheaterID;
    private String TheaterName;
    private String Capacity;

    public Theater_attr(String theaterID, String theaterName, String capacity) {
        TheaterID = theaterID;
        TheaterName = theaterName;
        Capacity = capacity;
    }

    public HashMap returnAttrCollect() {
        HashMap<String, String> RAC = new HashMap<String, String>();
        RAC.put("TheaterID", TheaterID);
        RAC.put("TheaterName", TheaterName);
        RAC.put("Capacity", Capacity);
        return RAC;
    }
}
