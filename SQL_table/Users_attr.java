package SQL_table;

import java.util.HashMap;

public class Users_attr {
    private String UserID;
    private String LoginName;
    private String Password;
    private String UserType;//0~2,分别为系统、管理员、用户

    public Users_attr(String userID, String loginName, String password, String userType) {
        UserID = userID;
        LoginName = loginName;
        Password = password;
        UserType = userType;
    }

    public String getUserID() {
        return UserID;
    }

    public String getLoginName() {
        return LoginName;
    }

    public String getPassword() {
        return Password;
    }

    public String getUserType() {
        return UserType;
    }

    public HashMap returnAttrCollect() {
        HashMap<String, String> RAC = new HashMap<String, String>();
        RAC.put("UserID", UserID);
        RAC.put("LoginName", LoginName);
        RAC.put("Password", Password);
        RAC.put("UserType", UserType);
        return RAC;
    }
}
