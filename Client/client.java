import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.SynchronousQueue;

public class client extends Thread{
    String userID=null;
    private int tryNum=4;
    public boolean linkable;
    public static SynchronousQueue messageQueue;

    public boolean login(String userID,String passwd)
    {
        return false;

    }

    public boolean sent(String request){
        return false;
    }

    public boolean receive(){
        return false;
    }

    public void run(){
        link();
    }

    public void link(){
        while (tryNum>0) {
            try {
                System.out.println("start");
                String serverHost = "localhost";//服务器地址
                Socket socket = new Socket(serverHost, 2333);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                InetAddress inetAddress = socket.getInetAddress();
                linkable = true;//连接成功
                tryNum=-1;//防止进入重新连接的循环
                System.out.println("success");
                dataOutputStream.writeUTF("qwe");
//                while (true){
//
//                }
            } catch (IOException e) {//尝试重连tryNum次
                e.printStackTrace();
                System.out.println(tryNum);
                tryNum--;
            }
        }
        linkable=false;//连接失败
        System.out.println("failed");
    }

    public static void main(String[] args){
        client client=new client();
        client.start();
    }
}
